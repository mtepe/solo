<?php

namespace App\Http\Controllers;

use App\Tutorial;
use App\User;
use Illuminate\Http\Request;

class DevController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dev');
    }

    public function index()
    {
        return view('dev.index');
    }

}
