<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\Tutorial;
use Illuminate\Http\Request;
use PDF;

class CertificateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ajax')->only( 'check');
    }

    public function show(Certificate $certificate)
    {

        $certificate = auth()->user()->certificate()->find($certificate->id);

        if(!$certificate){
            return redirect('/profil');
        }

        $data = ['certificate' => $certificate];
        $pdf = PDF::loadView('certificate/show', $data);
        $fileName = strtolower(str_replace(' ', '_', $certificate->user->name).'_'.$certificate->tutorial->name.'_sertifika'.'.pdf');
        return $pdf->download($fileName);
    }

    public function check(Tutorial $tutorial)
    {
        $user = auth()->user();
        $cert_check = Certificate::where('user_id', $user->id)->where('tutorial_id', $tutorial->id)->first();


        if (!$cert_check) {
            $url = FALSE;
            return $url;
        } else {
            return $url = route('certificate.show', ['certificate' => $cert_check->id]);
        }
    }
}
