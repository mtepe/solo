<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Dev\QuestionController;
use App\Lesson;
use App\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dev');
    }

    public function create(Lesson $lesson)
    {
        return view('dev.topic.create', compact(['lesson']));
    }

    public function store(Request $request)
    {
        $this->validateRequest($request);

        $countCurrent = Topic::where('lesson_id', $request->lesson_id)->count();

        $topic = Topic::create([
            'lesson_id' => $request['lesson_id'],
            'body' => $request['topic_body'],
            'position' => ($countCurrent + 1),
        ]);

        $q = new QuestionController();
        $q->store($topic, $request);

        return redirect('dev/lesson/'.$topic->lesson->id);
    }

    public function show(Topic $topic)
    {
        return view('dev.topic.show', compact(['topic']));
    }

    public function edit(Topic $topic)
    {
        return view('dev.topic.edit', compact('topic'));
    }

    public function update(Request $request, Topic $topic)
    {
        $data = request()->validate([
            'body' => 'required',
        ]);

        $topic->update($data);

        return redirect('dev/lesson/'.$topic->lesson->id);
    }

    public function destroy(Topic $topic)
    {

        $da = $topic->question->answers()->delete();
        $dq = $topic->question()->delete();
        $dt = $topic->delete();

        if($da && $dq && $da) {
            return redirect('dev/lesson/'.$topic->lesson->id);
        } else {
            return redirect('dev/lesson/'.$topic->lesson->id);
        }

    }

    public function validateRequest($request)
    {

        $request->validate([
            'lesson_id' => 'required',
            'topic_body' => 'required',
            'question_body' => 'required',
            'active' => 'min:1',
            'position' => '',
        ]);

        $type = $request['question_type'];

        if ($type == 'FILL') {
            $request->validate([
                'marked' => 'min:1',
            ]);
        } elseif ($type == 'QUIZ'){
            $request->validate([
                'answer' => 'required',
                'answers' => 'required',
            ]);
        } elseif ($type == 'TYPEIN'){
            $request->validate([
                'answer' => 'required',
            ]);
        }
    }

    public function updatePos()
    {
        foreach ($_POST['positions'] as $position) {
            Topic::find($position[0])->update(['position' => $position[1]]);
        }
    }

}
