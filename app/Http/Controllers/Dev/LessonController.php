<?php

namespace App\Http\Controllers\Dev;

use App\Lesson;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/*
 * 0-construct: checks for if user authorized and a developer
 * 1-create: Loads lesson create view
 * 2-store: Stores posted request
 * 3-show: Shows a single lesson
 * 4-edit: Loads edit page for selected lesson
 * 5-update: Updates posted request for lesson
 * 6-destroy: Deletes selected lesson
 * 7-validateRequest: Validates post request
 * 8-updatePos: Updates lesson position
 * */

class LessonController extends Controller
{

    public function create(Module $module)
    {
        return view('dev.lesson.create', compact('module'));
    }

    public function store(Request $request)
    {
        $this->valideteRequest();

        $countCurrent = Lesson::where('module_id', $request->module_id)->count();

        Lesson::create([
            'module_id' => $request->module_id,
            'name' => $request->name,
            'position' => ($countCurrent + 1),
        ]);

        return redirect('dev/module/'.request('module_id'));
    }

    public function show(Lesson $lesson)
    {
        return view('dev.lesson.show', compact('lesson'));
    }

    public function edit(Lesson $lesson)
    {
        return view('dev.lesson.edit', compact('lesson'));
    }

    public function update(Request $request, Lesson $lesson)
    {
        $data = $this->valideteRequest();

        $lesson->update($data);

        return redirect('dev/module/'.request('module_id'));
    }

    public function destroy(Lesson $lesson)
    {
        $lesson->delete();
        return redirect('dev/module/'.$lesson->module->id);
    }

    public function updatePos()
    {
        foreach ($_POST['positions'] as $position) {
            Lesson::find($position[0])->update(['position' => $position[1]]);
        }
    }

    public function valideteRequest()
    {
        return request()->validate([
            'module_id' => 'required',
            'name' => 'required',
            'position' => '',
        ]);
    }
}
