<?php

namespace App\Http\Controllers\Dev;

use App\Module;
use App\Tutorial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

/*
 * 0-construct: checks for if user authorized and a developer
 * 1-create: Loads module create view
 * 2-store: Stores posted request
 * 3-show: Shows a single module
 * 4-edit: Loads edit page for selected module
 * 5-update: Updates posted request for module
 * 6-destroy: Deletes selected module
 * 7-validateRequest: Validates post request
 * 8-storeImg: Stores and resizes image
 * 9-updatePos: Updates module position
 * */

class ModuleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dev');
    }

    public function create(Tutorial $tutorial)
    {
        return view('dev.module.create', compact(['tutorial']));
    }

    public function store(Request $request)
    {
        $this->validateRequest();

        $countCurrent = Module::where('tutorial_id', $request->tutorial_id)->count();

        $module = Module::create([
            'tutorial_id' => $request->tutorial_id,
            'name' => $request->name,
            'position' => ($countCurrent + 1),
        ]);

        $this->storeImg($module);

        return redirect('dev/module/'.$module->id);
    }

    public function show(Module $module)
    {
        return view('dev.module.show', compact('module'));
    }

    public function edit(Module $module)
    {
        return view('dev.module.edit', compact('module'));
    }

    public function update(Request $request, Module $module)
    {
        $module->update($this->validateRequest());

        $this->storeImg($module);

        return redirect('dev/tutorial/'.request('tutorial_id'));
    }

    public function destroy(Module $module)
    {
        $module->delete();

        return redirect('dev');
    }

    public function validateRequest()
    {
        return request()->validate([
            'name' => 'required',
            'tutorial_id' => '',
            'icon' => 'sometimes|file|image|max:5000',
            'position' => '',
        ]);

    }

    private function storeImg($module)
    {
        if (request()->has('icon')){
            $module->update([
                'icon' => request()->icon->store('uploads', 'public'),
            ]);
            $image = Image::make(public_path('storage/'.$module->icon))->fit(300, 300);
            $image->save();
        }


    }

    public function updatePos()
    {
        foreach ($_POST['positions'] as $position) {
            Module::find($position[0])->update(['position' => $position[1]]);
        }
    }
}
