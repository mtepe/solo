<?php

namespace App\Http\Controllers\Dev;

use App\Tutorial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

/*
 * 0-construct: checks for if user authorized and a developer
 * 1-index: Loads all tutorials with view
 * 2-create: Loads tutorial create view
 * 3-store: Stores posted request
 * 4-show: Shows a single tutorial
 * 5-edit: Loads edit page for selected tutorial
 * 6-update: Updates posted request for tutorial
 * 7-destroy: Deletes selected tutorial
 * 8-validateRequest: Validates post request
 * 9-storeImg: Stores and resizes image
 * 10-updatePos: Updates tutorial position
 * */

class TutorialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dev');
    }

    public function index()
    {
        $tutorials = Tutorial::orderBy('position', 'asc')->get();

        return view('dev.tutorial.index', compact(['tutorials']));
    }

    public function create()
    {
        return view('dev.tutorial.create');
    }

    public function store(Request $request)
    {
        $this->validateRequest();

        $countCurrent = Tutorial::get()->count();

        // TODO Short desc and long desc should be added
        $tutorial = Tutorial::create([
            'name' => $request->name,
            'position' => ($countCurrent + 1),
        ]);

        $this->storeImg($tutorial);

        return redirect('dev/tutorial/'.$tutorial->id);
    }

    public function show(Tutorial $tutorial)
    {
        return view('dev.tutorial.show', compact(['tutorial']));
    }

    public function edit(Tutorial $tutorial)
    {
        return view('dev.tutorial.edit', compact(['tutorial']));
    }

    public function update(Request $request, Tutorial $tutorial)
    {

        $tutorial->update($this->validateRequest());

        $this->storeImg($tutorial);

        return redirect('dev/tutorial/'.$tutorial->id);
    }

    public function destroy(Tutorial $tutorial)
    {
        $tutorial->delete();

        return redirect('dev');
    }

    public function validateRequest()
    {
        return request()->validate([
            'name' => 'required',
            'position' => 'sometimes',
            'logo' => 'sometimes|file|image|max:5000',
        ]);

    }

    private function storeImg($tutorial)
    {
        if (request()->has('logo')){
            $tutorial->update([
                'logo' => request()->logo->store('uploads', 'public'),
            ]);
            $image = Image::make(public_path('storage/'.$tutorial->logo))->fit(300, 300);
            $image->save();
        }
    }

    public function updatePos()
    {
        foreach ($_POST['positions'] as $position) {
            Tutorial::find($position[0])->update(['position' => $position[1]]);
        }
    }

}
