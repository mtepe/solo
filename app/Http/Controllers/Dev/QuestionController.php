<?php

namespace App\Http\Controllers\Dev;


use App\Http\Controllers\Controller;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Dev\AnswerController;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dev');
    }

    public function store($topic, $request)
    {
        $type = $request['question_type'];

        if ($type == 'FILL') {
            $this->createFill($topic, $request);
        } elseif ($type == 'QUIZ'){
            $this->createQuiz($topic, $request);
        } elseif ($type == 'TYPEIN'){
            $this->createTypeIn($topic, $request);
        }
    }

    public function update(Request $request, Question $question)
    {

        $this->validateInfo($request);

        $question->answers()->delete();
        $question->delete();
        $this->store($question->topic, $request);
        return redirect('dev/topic/'.$question->topic->id);

    }

    protected function createFill($topic, $request)
    {
        // Question body
        $string = $request['question_body'];

        // Define pattern
        $pattern = '~<mark>(.*?)</mark>~';

        //Find all matches
        preg_match_all($pattern, $string, $matches);

        // Replace each match with input
        foreach ($matches[1] as $answer) {
            $new_pattern = '~<mark>'.$answer.'</mark>~';
            $replace = '<input type="text" name="qs[]" maxlength="'.strlen($answer).'" size="'.strlen($answer).'" required>';
            $string = preg_replace($new_pattern, $replace, $string);
        }

        //Empty answers array to push
        $answers = [];

        foreach ($matches[1] as $match){
            array_push($answers, $match);
        }

        $question = Question::create([
            'topic_id' => $topic->id,
            'body' => $string,
            'type' => $request->question_type,
        ]);


        // Create answers in DB
        $a = new AnswerController();
        $a->store($question, $answers);

        return true;
    }

    protected function createQuiz($topic, $request)
    {
        $question = Question::create([
            'topic_id' => $topic->id,
            'body' => $request['question_body'],
            'answer' => $request['answer'],
            'type' => $request->question_type,
        ]);

        $a = new AnswerController();
        $a->store($question, $request['answers']);

        return true;
    }

    protected function createTypeIn($topic, $request)
    {
        Question::create([
            'topic_id' => $topic->id,
            'body' => $request['question_body'],
            'answer' => $request['answer'],
            'type' => $request->question_type,
        ]);

        return true;
    }

    protected function validateInfo($request)
    {
        $request->validate([
            'question_body' => 'required',
            'active' => 'min:1',
        ]);

        $type = $request['question_type'];

        if ($type == 'FILL') {
            $request->validate([
                'marked' => 'min:1',
            ]);
        } elseif ($type == 'QUIZ'){
            $request->validate([
                'answer' => 'required',
                'answers' => 'required',
            ]);
        } elseif ($type == 'TYPEIN'){
            $request->validate([
                'answer' => 'required',
            ]);
        }
    }
}
