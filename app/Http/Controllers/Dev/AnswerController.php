<?php

namespace App\Http\Controllers\Dev;

use App\Answer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dev');
    }

    public function store($question, $answers)
    {
        foreach ($answers as $answer) {
            Answer::create([
                'question_id' => $question->id,
                'body' => $answer,
            ]);
        }
    }

}

