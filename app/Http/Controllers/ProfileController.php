<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $profile = auth()->user()->profile;
        return view('profile/show', compact(['profile']));

    }

    public function update(Request $request)
    {
        $profile = auth()->user()->profile;
        $profile->update($this->validateRequest());
        $profile->user->update(['name' => $request->full_name]);
        $this->storeImg($profile);
        return redirect('profil');
    }

    public function validateRequest()
    {
        return request()->validate([
            'full_name' => 'required|min:3',
            'pic' => 'sometimes|file|image|max:3000',
        ]);
    }

    public function storeImg($profile)
    {
        if(request()->has('pic')) {
            $profile->update([
                'pic' => request()->pic->store('users', 'public'),
            ]);

            $image = Image::make(public_path('storage/'.$profile->pic))->fit('250', '250');
            $image->save();
        }
    }

}
