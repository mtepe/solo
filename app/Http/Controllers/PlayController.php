<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\Lesson;
use App\Module;
use App\Tutorial;
use App\User;
use Illuminate\Http\Request;

class PlayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ajax')->only( 'tutorial', 'module', 'lesson', 'update_lesson');
    }

    public function index(Tutorial $tutorial)
    {

        if(! auth()->user()->tutorials()->find($tutorial->id)) {
            $this->subInit($tutorial);
        }

        $ajax = '/play/tutorial/'.$tutorial->id;
        $tutorialName = $tutorial->name;
        return view('play.index', compact(['ajax', 'tutorialName']));
    }

    public function tutorial(Tutorial $tutorial)
    {
        $button = '';

        $modules = [];

        $user = auth()->user();

        foreach ($tutorial->modules()->orderBy('position', 'asc')->get() as $key => $module) {
            // Lesson status
            $status = $user->modules()->find($module->id)->pivot->status;
            /*
             * Status 0 is not completed, so view comes disabled
             * Status 1 is completed, so view comes enabled and all lessons are open
             * Status 2 is next, so view comes blue and enabled, but only first lesson is open
             */

            if ($status == 0){
                $module_view = '<div class="col-6 animated fadeIn"><div data-target="/play/module/'.$module->id.'" class="module play-disabled"><img src="/storage/'.$module->icon.'" class="module-logo"><p>'.$module->name.'</p></div></div>';
                array_push($modules, $module_view);
            } elseif ($status == 1){
                $module_view = '<div class="col-6 animated fadeIn"><div data-target="/play/module/'.$module->id.'" class="module"><img src="/storage/'.$module->icon.'" class="module-logo play-success"><p>'.$module->name.'</p></div></div>';
                array_push($modules, $module_view);
            } elseif ($status == 2){
                $module_view = '<div class="col-6 animated fadeIn"><div data-target="/play/module/'.$module->id.'" class="module"><img src="/storage/'.$module->icon.'" class="module-logo play-next"><p>'.$module->name.'</p></div></div>';
                array_push($modules, $module_view);
            }
        }

        $cert_check = Certificate::where('user_id', $user->id)->where('tutorial_id', $tutorial->id)->first();

        if ($cert_check) {
            $certificate = '<div class="col-8 animated fadeIn"><div class="certificate"  id="certificate" data-belongs="'.$tutorial->id.'"><img src="/img/cert/icon.png" class="certificate-logo"><p>Sertifika</p></div></div>';
        } else {
            $certificate = '<div class="col-8 animated fadeIn"><div class="certificate play-disabled" id="certificate" data-belongs="'.$tutorial->id.'"><img src="/img/cert/icon.png" class="certificate-logo"><p>Sertifika</p></div></div>';
        }

        $viewData = [
            'back' => $button,
            'areas' => $modules,
            'certificate' => $certificate,
        ];

        return json_encode($viewData);
    }

    public function module(Module $module)
    {
        $button = '<button id="backButton" data-target="/play/tutorial/'.$module->tutorial->id.'"><i class="fa fa-arrow-left"></i></button>';

        $lessons = [];

        $user = auth()->user();

        foreach ($module->lessons()->orderBy('position', 'asc')->get() as $key => $lesson) {
            // Lesson status
            $status = $user->lessons()->find($lesson->id)->pivot->status;
            /*
            * Status 0 is not completed, so view comes disabled
            * Status 1 is completed, so view comes enabled and all topics are open
            * Status 2 is next, so view comes blue and enabled, but only first topic is open
            */

            if ($status == 0){
                $lesson_view = '<div data-target="/play/lesson/'.$lesson->id.'" class="col-md-4 col-sm-6 lesson play-disabled animated bounceInUp"><div class="l-body"><p>'.($key+1).' / '.$module->lessons->count().'</p><h6>'.$lesson->name.'</h6></div><div class="l-foot">'.$lesson->topics->count().' Soru</div></div>';
                array_push($lessons, $lesson_view);

            } elseif ($status == 1){
                $lesson_view = '<div data-target="/play/lesson/'.$lesson->id.'" class="col-md-4 col-sm-6 lesson animated bounceInUp"><div class="l-body"><p>'.($key+1).' / '.$module->lessons->count().'</p><h6>'.$lesson->name.'</h6></div><div class="l-foot play-success">'.$lesson->topics->count().' Soru</div></div>';
                array_push($lessons, $lesson_view);
            } elseif ($status == 2){
                $lesson_view = '<div data-target="/play/lesson/'.$lesson->id.'" class="col-md-4 col-sm-6 lesson animated bounceInUp"><div class="l-body"><p>'.($key+1).' / '.$module->lessons->count().'</p><h6>'.$lesson->name.'</h6></div><div class="l-foot play-next">'.$lesson->topics->count().' Soru</div></div>';
                array_push($lessons, $lesson_view);
            }

        }

        $viewData = [
            'back' => $button,
            'areas' => $lessons,
        ];

        return json_encode($viewData);

    }

    public function lesson(Lesson $lesson)
    {
        // Back button to module lessons
        $button = '<button id="backButton" data-target="/play/module/'.$lesson->module->id.'"><i class="fa fa-arrow-left"></i></button>';

        // Empty topics array to fill
        $topics = [];

        // Logged in user
        $user = auth()->user();

        // Lesson active status
        $status = $user->lessons()->find($lesson->id)->pivot->status;

        // For every topic, return a topic link,related question link, topic content, related question content
        // If lesson is already completed, no link is disabled, if not completed, only first link couple is enabled, rest of the links are disabled


        foreach($lesson->topics()->orderBy('position', 'asc')->get() as $key => $topic) {
            if($key === 0 || $status == 1) {
                $tv['link'] = '<div data-key="'.$key.'" data-type="topic" id="tab'.$key.'" class="tab animated fadeIn"><i class="fa fa-play-circle"></i></div>';
                $tv['question_link'] = '<div data-key="'.$key.'" data-type="question" id="tab'.$key.'Q" data-active="0"  class="tab animated fadeIn"><i class="fa fa-question-circle"></i></div>';
            } else {
                $tv['link'] = '<div data-key="'.$key.'" data-type="topic" id="tab'.$key.'" class="tab play-disabled animated fadeIn"><i class="fa fa-play-circle"></i></div>';
                $tv['question_link'] = '<div data-key="'.$key.'" data-type="question" id="tab'.$key.'Q" data-active="0"  class="tab play-disabled animated fadeIn"><i class="fa fa-play-circle"></i></div>';
            }

            // Contents are loaded regardless of the status
            $tv['content'] = '<div id="topic'.$key.'" class=" animated fadeIn">'.$topic->body.'</div>';
            // Question body
            $tv['question']['body'] = '<div id="topic'.$key.'Question" class=" animated fadeIn">'.$topic->question->body.'</div></div>';
            // Question form
            $tv['question']['form'] = '<div class="questionForm animated fadeIn"><form id="topic'.$key.'QuestionForm" class="questionBody"><input type="hidden" name="id" value="'.$key.'"><input type="hidden" name="type" value="'.$topic->question->type.'"></form></div>';
            // Question correct answer (for type 2 & 3)
            $tv['question']['answer'] = $topic->question->answer;
            // Question answers (for type 1 & 2)
            $tv['question']['answers'] = $topic->question->answers->toArray();
            // Question type
            $tv['question']['type'] = $topic->question->type;
            // Topic skip button
            $tv['button'] = '<button type="button" class="next-button" data-key="'.$key.'" id="skip"><i class="fa fa-long-arrow-right"></i></button>';
            // Question check button
            $tv['question_button'] = '<button type="button" class="next-button" id="check" data-belongs="topic'.$key.'QuestionForm"><i class="fa fa-check"></i></button>';
            // Push topic
            array_push($topics, $tv);
        }

        // View data contains back button,topics,lesson status, module id and lesson id
        $viewData = [
            'back' => $button,
            'topics' => $topics,
            'status' => $status,
            'module' => $lesson->module->id,
            'lesson' => $lesson->id,
        ];

        // Return view data as a json array
        return json_encode($viewData);
    }

    public function update_lesson(Lesson $lesson)
    {
        $user = auth()->user();
        $status = $user->lessons()->find($lesson->id)->pivot->status;
        if($status == 1) {
            return $url = '/play/module/'.$lesson->module->id;
        } else {
            return $this->activate_lesson($user, $lesson, $status);
        }

    }

    public function activate_lesson($user, $lesson, $status)
    {
        // Set this lesson status = 1
        $user->lessons()->syncWithoutDetaching([
            $lesson->id => [
                'status' => 1,
            ],
        ]);

        // next lesson
        $next = Lesson::where('position', '>', $lesson->position)->where('module_id', $lesson->module->id)->orderBy('position', 'asc')->first();

        // There is next lesson :
        if($next){
            $user->lessons()->syncWithoutDetaching([
                $next->id => [
                    'status' => 2,
                ],
            ]);

            return $url = '/play/module/'.$lesson->module->id;
        } else {
            $status = $user->modules()->find($lesson->module->id)->pivot->status;
            return $this->activate_module($user, $lesson->module, $status);
        }
    }

    public function activate_module($user, $module, $status)
    {
        if($status == 1) {
            return $url = '/play/tutorial/'.$module->id;
        } else {
            // Set module status 1
            $user->modules()->syncWithoutDetaching([
                $module->id => [
                    'status' => 1,
                ],
            ]);

            // Next
            $next = Module::where('position', '>', $module->position)->where('tutorial_id', $module->tutorial_id)->orderBy('position', 'asc')->first();

            // There is next module:
            if($next) {
                // Set module status 2
                $user->modules()->syncWithoutDetaching([
                    $next->id => [
                        'status' => 2,
                    ],
                ]);

                return $url = '/play/tutorial/'.$module->tutorial->id;
            } else {
                $this->activate_tutorial($user, $module->tutorial);
                return $url = '/play/tutorial/'.$module->tutorial->id;
            }
        }

    }

    public function activate_tutorial($user, $tutorial)
    {
        $this->create_certificate($user, $tutorial);

        $user->tutorials()->syncWithoutDetaching([
            $tutorial->id => [
                'status' => 1,
            ]
        ]);

    }

    public function create_certificate(User $user, Tutorial $tutorial)
    {
        $check = Certificate::where('user_id', $user->id)->where('tutorial_id', $tutorial->id)->first();
        if(!$check) {
            $user->certificate()->create(['tutorial_id' => $tutorial->id]);
        }

    }

    public function subInit($tutorial)
    {
        $user = auth()->user();

        // Sync with tutorial
        $user->tutorials()->syncWithoutDetaching([
            $tutorial->id => [
                'status' => 0,
            ]
        ]);

        // Sync with modules
        foreach ($tutorial->modules()->orderBy('position', 'asc')->get() as $key => $module) {
            // Set first modules' status 2, so its enabled
            if ($key == 0) {
                $user->modules()->syncWithoutDetaching([
                    $module->id => [
                        'status' => 2,
                    ],
                ]);
            } else {
                $user->modules()->syncWithoutDetaching([
                    $module->id => [
                        'status' => 0,
                    ],
                ]);
            }

            // Sync with lessons
            foreach ($module->lessons as $key => $lesson) {
                // Set first lessons' status 2, so its enabled
                if ($key == 0) {
                    $user->lessons()->syncWithoutDetaching([
                        $lesson->id => [
                            'status' => 2,
                        ]
                    ]);
                } else {
                    $user->lessons()->syncWithoutDetaching([
                        $lesson->id => [
                            'status' => 0,
                        ]
                    ]);
                }

            }
        }
    }

    public function reset_progress(Tutorial $tutorial)
    {
        $this->subInit($tutorial);
        return  redirect('/play/'.$tutorial->id);
    }

}
