<?php

namespace App\Http\Controllers;

use App\Tutorial;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function courses()
    {
        return view('courses');
    }

    public function course(Tutorial $tutorial)
    {
        return view('course', compact('tutorial'));
    }

    public function emailSub()
    {
        \App\Subscription::create(['email' => request()->email]);
        return redirect('/');
    }
}
