<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
    protected $guarded = [];

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function users()
    {
        return $this->belongsToMany(\App\User::class)->withPivot(['status'])->withTimestamps();
    }

    public function certificate()
    {
        return $this->hasOne(Certificate::class);
    }
}
