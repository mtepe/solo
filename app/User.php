<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tutorials()
    {
        return $this->belongsToMany(\App\Tutorial::class)->withPivot(['status'])->withTimestamps();
    }

    public function modules()
    {
        return $this->belongsToMany(\App\Module::class)->withPivot(['status'])->withTimestamps();
    }

    public function lessons()
    {
        return $this->belongsToMany(\App\Lesson::class)->withPivot(['status'])->withTimestamps();
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function certificate()
    {
        return $this->hasOne(Certificate::class);
    }

}
