<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $guarded = [];

    public function tutorial()
    {
        return $this->belongsTo(Tutorial::class);
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }

    public function users()
    {
        return $this->belongsToMany(\App\User::class)->withPivot(['status'])->withTimestamps();
    }
}
