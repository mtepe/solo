<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Şifre en az 8 karakter olmalı ve tekrar ile uyuşmalı.',
    'reset' => 'Şifreniz yenilendi!',
    'sent' => 'Eposta adresinize şifre sıfırlama linki gönderdik!',
    'token' => 'Bu şifre sıfırlama tokeni geçersiz.',
    'user' => "Bu email adresiyle bir kullanıcı bulunamadı.",

];
