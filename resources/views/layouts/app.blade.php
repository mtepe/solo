<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Logo -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo/logo600.png') }}" />

    <title>@yield('title', 'Egitben')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap&subset=latin-ext"
          rel="stylesheet">

    <!-- Font Awesome CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Animate -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>

<div id="app">
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark px-md-5 myNav solid">
            <a class="navbar-brand pl-md-5" href="{{ url('/') }}">
                <img src="{{ asset('img/logo/logo600.png') }}" height="40">{{ config('gitben', 'gitben') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto pr-md-5">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home') }}">{{ __('Ana') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('courses') }}">{{ __('Kurslar') }}</a>
                    </li>
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Giriş') }}</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('profile.index') }}">
                                {{ Auth::user()->name }} <i class="ml-1 fa fa-user-o"></i>
                            </a>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </div>


    <main>
        @yield('content')
    </main>

    <!-- FOOTER -->

    <footer class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 my-2">
                    <!-- social -->
                    <a href="#" class="row social">
                        <div class="col-2">
                            <p><i class="fa fa-twitter"></i></p>
                        </div>
                        <div class="col-8">
                            <p>@egitben</p>
                        </div>
                    </a>
                    <!-- social -->
                    <!-- social -->
                    <a href="#" class="row social">
                        <div class="col-2">
                            <i class="fa fa-facebook"></i>
                        </div>
                        <div class="col-8">
                            <p>@egitben</p>
                        </div>
                    </a>
                    <!-- social -->
                    <!-- social -->
                    <a href="#" class="row social">
                        <div class="col-2">
                            <i class="fa fa-instagram"></i>
                        </div>
                        <div class="col-8">
                            <p>@egitben</p>
                        </div>
                    </a>
                    <!-- social -->
                </div>

                <div class="col-lg-4 my-2 company">
                    <div class="logo"><img src="{{ asset('img/logo/logo600.png') }}" height="50"> gitben</div>
                    <p class="py-2">Yeni kurslardan haberdar olmak için abone olun: </p>
                    <form action="{{ route('home.emailSub') }}">
                        <div class="input-group mb-3">
                            <input type="email" name="email" class="form-control" placeholder="Abone ol" required>
                            <div class="input-group-append">
                                <button class="btn btn-light" type="submit"><i class="fa fa-long-arrow-right"></i></button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-4 my-2">
                    <ul>
                        <li><a href="{{ route('courses') }}">Kurslar</a></li>
                        <li><a href="{{ route('course', ['tutorial' => 1]) }}">HTML</a></li>
                        <li><a href="{{ route('course', ['tutorial' => 2]) }}">CSS</a></li>
                        <li><a href="{{ route('course', ['tutorial' => 3]) }}">Javascript</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </footer>

</div>

<!-- Scripts -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Scripts -->
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<!-- Scripts -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

@yield('script')

</body>
</html>
