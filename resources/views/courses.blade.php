@extends('layouts.app')
@section('title', 'Kurslar - Eğitben')

@section('content')

    <div class="all-courses section-padding animated fadeIn">
        <div class="container py-5">
            <div class="row">

                <!-- a course -->
                <div class="col-lg-6 my-2">
                    <div class="a-course">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ asset('img/courses/html.png') }}" class="w-100">
                            </div>
                            <div class="col-9">
                                <h4>HTML Temelleri</h4>
                                <p>
                                    Bu ücretsiz kurs size HTML ile nasıl web sayfası tasarlayabileceğinizi öğretir.
                                    Bir çok pratik ve konuyla birlikte gerçek bir HTML sayfası yazmayı hemen öğrenebilirsiniz.
                                </p>
                                <a href="{{ route('course', ['tutorial' => 1]) }}" class="myBtn myBtnPurple">Detaylar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /a course/ -->

                <!-- a course -->
                <div class="col-lg-6 my-2">
                    <div class="a-course">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ asset('img/courses/python.png') }}" class="w-100">
                            </div>
                            <div class="col-9">
                                <h4>Python Dersleri</h4>
                                <p>
                                    Python öğrenin, günümüzün en çok aranın programlama dillerinden biri! Pratiklerle birlikte Python yazmayı öğrenin ve gelişimi kendiniz görün.
                                </p>
                                <a href="#" class="myBtn myBtnPurple">Detaylar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /a course/ -->

                <!-- a course -->
                <div class="col-lg-6 my-2">
                    <div class="a-course">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ asset('img/courses/js.png') }}" class="w-100">
                            </div>
                            <div class="col-9">
                                <h4>Javascript Dersleri</h4>
                                <p>
                                    Bu kursumuzla Javascript'in bütün temel konularını öğrenin.
                                    Web sitenizi daha interaktif bir hâle getirme, sayfanın içeriğini değiştirme ve çok
                                    daha fazlası.
                                </p>
                                <a href="#" class="myBtn myBtnPurple">Detaylar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /a course/ -->

                <!-- a course -->
                <div class="col-lg-6 my-2">
                    <div class="a-course">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ asset('img/courses/php.png') }}" class="w-100">
                            </div>
                            <div class="col-9">
                                <h4>PHP Dersleri</h4>
                                <p>
                                    PHP ile dinamik web siteleri oluşturabilir, web siteleri geliştirebilir ve dinamik içerik üretebilirsiniz.Günümüzün en yaygın web programlama dilini şimdi öğrenin!

                                </p>
                                <a href="{{ route('course', ['tutorial' => 1]) }}" class="myBtn myBtnPurple">Detaylar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /a course/ --><!-- a course -->
                <div class="col-lg-6 my-2">
                    <div class="a-course">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ asset('img/courses/css.png') }}" class="w-100">
                            </div>
                            <div class="col-9">
                                <h4>CSS Temelleri</h4>
                                <p>
                                    CSS kursumuzla web sayfalarının stil ve görünümünde ustalaşabilirsiniz.
                                    Bir çok pratik ve konu size CSS'le harika bir web sitesi tasarlamanızda yardımcı olacak.
                                </p>
                                <a href="#" class="myBtn myBtnPurple">Detaylar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /a course/ -->

                <!-- a course -->
                <div class="col-lg-6 my-2">
                    <div class="a-course">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ asset('img/courses/sql.png') }}" class="w-100">
                            </div>
                            <div class="col-9">
                                <h4>SQL Temelleri</h4>
                                <p>
                                    Bu kursla bir dizi SQL konusu öğrenebilirsiniz.Veri görüntüleme, güncelleme, filtreleme,
                                    fonksiyonlar, sorgular, tablo oluşturma, güncelleme ve daha fazlası!
                                </p>
                                <a href="#" class="myBtn myBtnPurple">Detaylar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /a course/ -->


            </div>
        </div>
    </div>

@endsection
