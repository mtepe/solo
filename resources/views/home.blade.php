@extends('layouts.app')

@section('content')

    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-7  header-left">
                    <h2>Kodlamayı ÜCRETSİZ öğrenin!</h2>
                    <p>
                        Egitben'in ücretsiz kurslarıyla programlama dillerini kodlamayı ücretsiz öğrenebilirsiniz.
                        Anlaşılır konu anlatımları ve sorularla aynı anda hem öğrenin, hem pratik yapın.
                    </p>
                    <a href="#courses" class="myBtn myBtnBlue">Hemen Öğrenmeye Başla</a>
                </div>
                <div class="col-lg-4">
                    <div class="owl-carousel owl-theme header-slider">
                        <div class="item"><img src="{{ asset('img/screen-3.jpg') }}"></div>
                        <div class="item"><img src="{{ asset('img/screen-4.jpg') }}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container courses py-5 text-center" id="courses">
        <div class="row">
            <!-- a course -->
            <div class="col-lg-4 py-3">
                <a href="{{ route('play', ['tutorial' => 1]) }}">
                    <div class="course">
                        <img src="{{ asset('img/courses/html.png') }}" alt="html kursu" class="course-img">
                        <h4 class="course-title mt-3">HTML Kursu</h4>
                        <p class="course-summary">
                            Bu ücretsiz kurs size HTML ile nasıl web sayfası tasarlayabileceğinizi öğretir.
                            Bir çok pratik ve konuyla birlikte gerçek bir HTML sayfası yazmayı hemen öğrenebilirsiniz.
                        </p>
                        <a href="{{ route('play', ['tutorial' => 1]) }}" class="myBtn myBtnPurple">Kursa Başla</a>
                    </div>
                </a>
            </div>
            <!-- /a course/ -->
            <!-- a course -->
            <div class="col-lg-4 py-3">
                <a href="#">
                    <div class="course">
                        <img src="{{ asset('img/courses/css.png') }}" alt="css kursu" class="course-img">
                        <h4 class="course-title mt-3">CSS Kursu</h4>
                        <p class="course-summary">
                            CSS kursumuzla web sayfalarının stil ve görünümünde ustalaşabilirsiniz.
                            Bir çok pratik ve konu size CSS'le harika bir web sitesi tasarlamanızda yardımcı olacak.
                        </p>
                        <a href="#" class="myBtn myBtnPurple">Yakında!</a>
                    </div>
                </a>
            </div>
            <!-- /a course/ -->
            <!-- a course -->
            <div class="col-lg-4 py-3">
                <a href="#">
                    <div class="course">
                        <img src="{{ asset('img/courses/js.png') }}" alt="js kursu" class="course-img">
                        <h4 class="course-title mt-3">Javascript Kursu</h4>
                        <p class="course-summary">
                            Bu kursumuzla Javascript'in bütün temel konularını öğrenin.
                            Web sitenizi daha interaktif bir hâle getirme, sayfanın içeriğini değiştirme ve çok
                            daha fazlası.
                        </p>
                        <a href="#" class="myBtn myBtnPurple">Yakında!</a>
                    </div>
                </a>
            </div>
            <!-- /a course/ -->
        </div>
    </div>

    <div class="process py-5">
        <div class="container">
            <!--Carousel Wrapper-->
            <div id="carousel-thumb" class="carousel slide carousel-thumbnails" data-ride="carousel">
                <!--Slides-->
                <div class="carousel-inner pb-5" role="listbox">
                    <!-- an item -->
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-md-6 py-lg-5 animated fadeIn slower">
                                <h2>Öğrenmeye başla</h2>
                                <p>
                                    Dilediğiniz kursu seçip anında öğrenmeye başlayabilirsiniz.
                                    <br>
                                    Eğitben, Web'den mobile bir çok alanda konu anlatımı sağlar.
                                </p>
                            </div>
                            <div class="col-md-6 animated fadeIn delay-1s slow">
                                <img src="{{ asset('img/rwd.png') }}" class="w-100">
                            </div>
                        </div>
                    </div>
                    <!-- /an item/ -->

                    <!-- an item -->
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-6 py-lg-5 animated fadeIn slower">
                                <h2>Konulara bak</h2>
                                <p>
                                    Kısa ve öz konu anlatımlarıyla hızlı ve efektif bir şekilde öğrenebilirsiniz.
                                    Her ders konulara hakim olmanız için dikkatli bir şekilde seçilmiştir.
                                </p>
                            </div>
                            <div class="col-md-6 animated fadeIn delay-1s slow">
                                <img src="{{ asset('img/rwd.png') }}" class="w-100">
                            </div>
                        </div>
                    </div>
                    <!-- /an item/ -->
                    <!-- an item -->
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-6 py-lg-5 animated fadeIn slower">
                                <h2>Pratik yap</h2>
                                <p>
                                    Konu anlatımlarıyla el ele giden soru-cevaplarla hızlı ve etkili bir biçimde pratik yapabilirsiniz.
                                    <br>
                                    Eğitben farklı soru türleriyle hafızanızın zinde kalmasına yardımcı olur.
                                </p>
                            </div>
                            <div class="col-md-6 animated fadeIn delay-1s slow">
                                <img src="{{ asset('img/rwd.png') }}" class="w-100">
                            </div>
                        </div>
                    </div>
                    <!-- /an item/ -->
                    <!-- an item -->
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-6 py-lg-5 animated fadeIn slower">
                                <h2>Sertifikanı al</h2>
                                <p>
                                    Seçtiğiniz kursu tamamladığınızda, Eğitben'in kurs sertifikasına hak kazanmış olursunuz.
                                    Yakınlarınızla dilediğiniz gibi paylaşabilirsiniz!
                                </p>
                            </div>
                            <div class="col-md-6 animated fadeIn delay-1s slow">
                                <img src="{{ asset('img/rwd.png') }}" class="w-100">
                            </div>
                        </div>
                    </div>
                    <!-- /an item/ -->
                </div>
                <!--/.Slides-->
                <!--/.Controls-->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-thumb" data-slide-to="0" class="active">
                        Öğrenmeye başla
                    </li>
                    <li data-target="#carousel-thumb" data-slide-to="1">
                        Konulara bak
                    </li>
                    <li data-target="#carousel-thumb" data-slide-to="2">
                        Pratik yap
                    </li>
                    <li data-target="#carousel-thumb" data-slide-to="3">
                        Sertifikanı al
                    </li>
                </ol>
            </div>
            <!--/.Carousel Wrapper-->
        </div>
    </div>


    <!-- TESTIMONIALS -->

    <div class="testimonials py-5 text-center">
        <div class="container">
            <h2>Bizle ilgili ne dediler:</h2>
            <div class="row">
                <!-- person -->
                <div class="col-lg-4 pb-3">
                    <div class="person-quote">
                        <p>
                            Probably the best way to use my phone. This is a revolutionary way to learn code. The comments on each question/section adds a real world perspective.
                        </p>
                    </div>
                    <img src="{{ asset('img/people/person1.jpg') }}" class="person-img">
                    <h6 class="person-name pt-2">Jane Doe</h6>
                    <p class="person-course">HTML</p>
                </div>
                <!-- /person/ -->
                <!-- person -->
                <div class="col-lg-4 pb-3">
                    <div class="person-quote">
                        <p>
                            Wow... I mean... WOW! Boys and girls, just go for it... I mean, it's just so simple, so pretty and well organized. I've been looking for free, self learning tools all over for all kinds of stuff and this is by far, the best one.
                        </p>
                    </div>
                    <img src="{{ asset('img/people/person2.jpg') }}" class="person-img">
                    <h6 class="person-name pt-2">Marry Doe</h6>
                    <p class="person-course">Python</p>
                </div>
                <!-- /person/ -->
                <!-- person -->
                <div class="col-lg-4 pb-3">
                    <div class="person-quote">
                        <p>
                            Appreciations! Superb application with step-by-step teaching. You are making a lot of impacts especially in Ghana, Africa. I couldn't afford the university to learn to code. Thanks so much. Keep it up.
                        </p>
                    </div>
                    <img src="{{ asset('img/people/person3.jpg') }}" class="person-img">
                    <h6 class="person-name pt-2">James Smith</h6>
                    <p class="person-course">PHP</p>
                </div>
                <!-- /person/ -->
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Scripts -->
    <script src="{{ asset('js/home.js') }}"></script>
@endsection
