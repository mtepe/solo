@extends('layouts.app')
@section('title', $profile->full_name.'  - Egitben')

@section('content')
<div class="profile section-padding">
    <div class="container py-5">
        <div class="row">

            <div class="col-lg-5 mb-3 animated fadeIn">
                <div class="user-info myBox">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{ asset('storage/'.$profile->pic) }}" class="w-100">
                        </div>
                        <div class="col-8">
                            <h4>{{ $profile->full_name }}</h4>
                            <div class="d-flex justify-content-between pt-4">
                                <a class="btn btn-outline-secondary mr-2" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Çık') }}
                                </a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                <button class="btn btn-success" id="editProfile">Profili Düzenle</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="user-edit myBox mt-3 animated pulse" >
                    <form action="{{ route('profile.update') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <input type="file" name="pic" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" name="full_name" placeholder="Kullanıcı ismi" class="form-control"
                                   value="{{ old('full_name') ?? $profile->full_name }}">
                        </div>
                        <div>
                            <button type="button" class="btn btn-sm btn-danger" id="editDismiss">Vazgeç</button>
                            <button type="submit" class="btn btn-sm btn-info">Kaydet</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-7 animated fadeIn">
                <div class="myCourses myBox">
                    <div>
                        <h6>Takip Ettiğim Kurslar</h6>
                        <div class="row py-2">
                            @foreach($profile->user->tutorials as $tutorial)
                                <div class="myCourseSingle text-center col-sm-2">
                                    <a href="{{ route('play', ['tutorial' => $tutorial]) }}"><img src="{{ asset('storage/'.$tutorial->logo) }}" class="animated pulse infinite"></a>
                                    <div class="my-2">
                                        <a href="#" class="text-dark" id="resetProgress" data-target="{{ route('play.reset', ['tutorial' => $tutorial]) }}">sıfırla</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    <div>
                        <h6>Sertifikalarım</h6>
                        <div class="row my-3">
                            @foreach($profile->user->certificate()->get() as $certificate)
                                <div class="col-lg-12 d-flex my-2">
                                    <div class="col-3">
                                        <img src="{{ asset('storage/'.$certificate->tutorial->logo) }}" class="w-100">
                                    </div>
                                    <div class="col-9">
                                        <h5>{{ $certificate->tutorial->name }} Dersleri</h5>
                                        <p class="text-muted">Verilme tarihi: {{ date('d.m.Y', strtotime($certificate->created_at)) }}</p>
                                        <p><b>İndir:</b> <a href="{{ route('certificate.show', ['certificate' => $certificate]) }}">PDF</a> - <a href="#">Resim</a> </p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).on('click', '#editProfile', function() {
            $('.user-edit').show();
        });

        $(document).on('click', '#editDismiss', function() {
            $('.user-edit').hide();
        });

        $(document).on('click', '#resetProgress', function() {
            var ask = window.confirm("Bu kursu sıfırlamak istediğinize emin misiniz ?");
            if (ask) {
                window.location.href = $(this).attr('data-target');
            }
        });

    </script>
@endsection
