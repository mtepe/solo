<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Logo -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo/logo600.png') }}" />

    <title>@yield('title', 'Egitben')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap&subset=latin-ext"
          rel="stylesheet">

    <!-- Font Awesome CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Animate -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/play.css') }}" rel="stylesheet">
</head>
<body>

<div id="app">
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark px-md-5 myNav solid">
            <a class="navbar-brand pl-md-5" href="{{ url('/') }}">
                <img src="{{ asset('img/logo/logo600.png') }}" height="40">{{ config('gitben', 'gitben') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav ml-5">
                    <li class="nav-item navbar-brand">
                        @yield('tutorial')
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto pr-md-5">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('profile.index') }}">
                            {{ Auth::user()->name }} <i class="ml-1 fa fa-user-o"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>


    <main>
        @yield('content')
    </main>


</div>

<!-- Jquery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- Custom -->
<script src="{{ asset('js/play.js') }}"></script>
<!-- Custom -->
<script src="{{ asset('js/swal.js') }}"></script>

@yield('script')

<script>

    // Load all modules
    $(document).ready(function() {
        var url = '<?= $ajax ?>';

        load_areas(url);
    });
</script>

</body>
</html>
