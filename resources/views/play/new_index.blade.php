@extends('play.layouts.new_app')
@section('tutorial', $tutorialName)
@section('title', 'Oyna')

@section('content')

<div class="container">

    <div class="w-100 back-container">
        <div class="back-area offset-lg-2">
        </div>
    </div>

    <div class="playground col-lg-8 offset-lg-2">

        <!-- tab section -->
        <div class="tab-content d-flex justify-content-center">

            <div class="tab-list">

            </div>

        </div>
        <!-- /tab section/ -->

        <div class="row d-flex justify-content-center" id="playInner">


        </div>

        <div class="play-body my-3 ">

        </div>

        <div class="play-footer d-flex justify-content-center">

        </div>

    </div>
</div>
@endsection


