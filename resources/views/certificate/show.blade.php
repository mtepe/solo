<!DOCTYPE html>
<html>
<head>
    <title>{{ $certificate->user->name.' '.$certificate->tutorial->name.' Sertifikası' }}</title>
    <style>

        @page { size: 768px 1280px landscape; }

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        body {
            font-family: DejaVu Sans;
            border: 20px double #0984e3;
        }

        .container {
            position: relative; background-color: #fff;
        }

        .top {
            display: block; position: absolute; top: 0;
        }

        .inner {
            background-color: transparent; margin: 0; padding: 0; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); z-index: 999; text-align: center;
        }

        .logo {
            width: 100px; margin: 0 0 25px;
        }

        .user {
            margin: 10px 0; font-size: 52px;
        }

        .para {
            margin: 10px 0; font-size: 16px;
        }

        .course {
            margin: 10px 0; font-size: 40px;
        }

        .date {
            width: 200px; border-bottom: 3px solid #000; display: inline-block; margin: 50px 50px 0; padding-bottom: 5px; font-size: 16px; font-weight: 500;
        }

        .sign {
            width: 200px; border-bottom: 3px solid #000; display: inline-block; margin: 0 50px 0; padding-bottom: 5px; font-size: 16px; font-weight: 500;
        }

        .signature {
            opacity: 0.8;
            font-family: Edwardian Script ITC;
        }


        .bottom {
            display: block; position: absolute; bottom: 0;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="inner">
        <img src="{{ public_path().'/img/logo/logo600.png' }}" class="logo">
        <h1 class="user">{{ $certificate->user->name }}</h1>
        <p class="para">adlı kişi</p>
        <h2 class="course">{{ $certificate->tutorial->name }} KURSUNU</h2>
        <p class="para">başarıyla tamamlamıştır</p>

        <div class="date">{{ date('d.m.Y', strtotime($certificate->created_at)) }}</div>
        <div class="sign"><span class="signature">Muhammed Tepe</span></div>
    </div>
</div>

</body>
</html>


