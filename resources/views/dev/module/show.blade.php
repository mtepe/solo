@extends('dev.layouts.app')

@section('title', $module->name.' Kursu')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-4">
                <h2>{{ $module->name }}</h2>
                <img src="{{ asset('storage/'.$module->icon) }}" alt="{{ $module->name }}" class="img-fluid img-thumbnail my-3" style="width: 150px;">
                <p>
                    <b>Kurs: </b>
                    <a href="{{ route('tutorial.show', ['tutorial' => $module->tutorial]) }}">
                        {{ $module->tutorial->name }}
                    </a>
                </p>
                <p><a href="{{ route('module.edit', ['module' => $module]) }}" class="btn btn-info">Düzenle</a></p>
                <form action="{{ route('module.destroy', ['module' => $module]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Sil</button>
                </form>
            </div>
            <div class="col-4">
                <h3>Dersler</h3>
                <p>
                    <a href="{{ route('lesson.create', ['module' => $module]) }}" class="btn btn-sm btn-outline-secondary">
                        Yeni Ders
                    </a>
                </p>

                <table>
                    <tbody>
                    @foreach($module->lessons()->orderBy('position', 'asc')->get() as $lesson)
                        <tr data-index="{{ $lesson->id }}" data-position="{{ $lesson->position }}">
                            <td>
                                <a href="{{ route('lesson.show', ['lesson' => $lesson->id]) }}">{{ $lesson->name }}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @foreach($module->lessons()->get() as $lesson)
                    <p></p>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('table tbody').sortable({
                update: function (event, ui) {
                    $(this).children().each(function (index) {
                        if($(this).attr('data-position') != (index+1)) {
                            $(this).attr('data-position', (index+1)).addClass('updated');
                        }
                    });

                    saveNewPositions();
                }
            });

        });

        function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
                positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
                $(this).removeClass('updated');
            });

            $.ajax({
                url: '{{ route('lesson.updatePos') }}',
                method: 'post',
                dataType: 'text',
                data: {
                    update: 1,
                    positions: positions,
                    _token: '{{csrf_token()}}',
                }, success: function (response) {
                    console.log(response);
                }
            });
        }
    </script>
@endsection
