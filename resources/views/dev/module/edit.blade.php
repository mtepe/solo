@extends('dev.layouts.app')

@section('title', $module->name.' Modülü Düzenleniyor')

@section('content')
    <div class="container">
        <h2>{{ $module->name }} Modülü Düzenle</h2>
        <form action="{{ route('module.update', ['module' => $module]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <input type="hidden" name="tutorial_id" value="{{ $module->tutorial->id }}">
            <div class="form-group">
                <input type="text" name="name" value="{{ $module->name }}" class="form-control" placeholder="Modül ismi" required>
                <p><small>{{ $errors->first('name') }}</small></p>
            </div>
            <div class="form-group d-flex flex-column">
                <label for="icon">İkon</label>
                <input type="file" name="icon" class="form-control">
                <p><small>{{ $errors->first('icon') }}</small></p>
            </div>
            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>
@endsection
