@extends('dev.layouts.app')

@section('title', $tutorial->name.' Modülü Ekle')

@section('content')
    <div class="container">
            <h2>{{ $tutorial->name }} Modülü Ekle</h2>
            <form action="{{ route('module.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="tutorial_id" value="{{ $tutorial->id }}">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Modül ismi" required>
                    <p><small>{{ $errors->first('name') }}</small></p>
                </div>
                <div class="form-group d-flex flex-column">
                    <label for="icon">İkon</label>
                    <input type="file" name="icon" class="form-control">
                    <p><small>{{ $errors->first('icon') }}</small></p>
                </div>
                <input type="hidden" name="position" value="0">
                <button type="submit" class="btn btn-primary">Ekle</button>
            </form>
    </div>
@endsection
