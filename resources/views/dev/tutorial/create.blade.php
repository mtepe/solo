@extends('dev.layouts.app')

@section('title', 'Yeni Kurs Oluştur')

@section('content')
    <div class="container">
            <h2>Yeni Kurs Ekle</h2>
            <form action="{{ route('tutorial.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Kurs ismi">
                    <p><small>{{ $errors->first('name') }}</small></p>
                </div>
                <div class="form-group d-flex flex-column">
                    <label for="logo">Logo</label>
                    <input type="file" name="logo" class="form-control">
                    <p><small>{{ $errors->first('logo') }}</small></p>
                </div>
                <input type="hidden" name="position" value="0">
                <button type="submit" class="btn btn-primary">Ekle</button>
            </form>
    </div>
@endsection
