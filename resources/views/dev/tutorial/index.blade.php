@extends('dev.layouts.app')

@section('title', 'Tüm Kurslar')

@section('content')
    <div class="container">
        <h2>Tüm Kurslar</h2>

        <a href="{{ route('tutorial.create') }}" class="btn btn-sm btn-outline-secondary">Kurs Oluştur</a>

        <hr>

        <table class="table table-hover table-bordered table-active">
            <tbody>
            @foreach($tutorials as $tutorial)
                <tr data-index="{{ $tutorial->id }}" data-position="{{ $tutorial->position }}">
                    <td><a href="{{ route('tutorial.show', ['tutorial' => $tutorial])  }}">{{ $tutorial->name }}</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('table tbody').sortable({
                update: function (event, ui) {
                   $(this).children().each(function (index) {
                       if($(this).attr('data-position') != (index+1)) {
                           $(this).attr('data-position', (index+1)).addClass('updated');
                       }
                   });

                    saveNewPositions();
                }
            });

        });

        function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
                positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
                $(this).removeClass('updated');
            });

            $.ajax({
                url: '{{ route('tutorial.updatePos') }}',
                method: 'post',
                dataType: 'text',
                data: {
                    update: 1,
                    positions: positions,
                    _token: '{{csrf_token()}}',
                }, success: function (response) {
                    console.log(response);
                }
            });
        }
    </script>
@endsection
