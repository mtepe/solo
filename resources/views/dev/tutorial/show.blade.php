@extends('dev.layouts.app')

@section('title', $tutorial->name.' Kursu')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-4">
                <h2>{{ $tutorial->name }}</h2>
                <img src="{{ asset('storage/'.$tutorial->logo) }}" alt="{{ $tutorial->name }}" class="img-fluid img-thumbnail my-3" style="width: 150px;">

                <p><a href="{{ route('tutorial.edit', ['tutorial' => $tutorial]) }}" class="btn btn-info">Düzenle</a></p>
                <form action="{{ route('tutorial.destroy', ['tutorial' => $tutorial]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Sil</button>
                </form>
            </div>
            <div class="col-4">
                <h3>Modüller</h3>
                <p><a href="{{ route('module.create', ['tutorial' => $tutorial]) }}"
                      class="btn btn-outline-secondary btn-sm">Yeni Modül</a></p>

                <table>
                    <tbody>
                        @foreach($tutorial->modules()->orderBy('position', 'asc')->get() as $module)
                            <tr data-index="{{ $module->id }}" data-position="{{ $module->position }}">
                                <td>
                                    <a href="{{ route('module.show', ['module' => $module->id]) }}">
                                        <img src="{{ asset('storage/'.$module->icon) }}" class="img-fluid img-thumbnail" style="width: 50px;">
                                        {{ $module->name }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('table tbody').sortable({
                update: function (event, ui) {
                    $(this).children().each(function (index) {
                        if($(this).attr('data-position') != (index+1)) {
                            $(this).attr('data-position', (index+1)).addClass('updated');
                        }
                    });

                    saveNewPositions();
                }
            });

        });

        function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
                positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
                $(this).removeClass('updated');
            });

            $.ajax({
                url: '{{ route('module.updatePos') }}',
                method: 'post',
                dataType: 'text',
                data: {
                    update: 1,
                    positions: positions,
                    _token: '{{csrf_token()}}',
                }, success: function (response) {
                    console.log(response);
                }
            });
        }
    </script>
@endsection
