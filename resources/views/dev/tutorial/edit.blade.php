@extends('dev.layouts.app')
@section('title', $tutorial->name.' Kursu Düzenleniyor')

@section('content')
    <div class="container">
        <h2>{{ $tutorial->name }} Düzenle</h2>
        <form action="{{ route('tutorial.update', ['tutorial' => $tutorial]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <input type="text" name="name" class="form-control" value="{{ $tutorial->name }}" placeholder="Kurs ismi">
                <p><small>{{ $errors->first('name')}}</small></p>
            </div>
            <div class="form-group d-flex flex-column">
                <label for="logo">Logo</label>
                <input type="file" name="logo" class="form-control">
                <p><small>{{ $errors->first('logo') }}</small></p>
            </div>
            <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
    </div>
@endsection
