@extends('dev.layouts.app')

@section('title', $lesson->name.'\'e Konu Anlatımı Ekle')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-6">

                <h2>{{ $lesson->name }} - Konu Anlatımı Ekle </h2>
            </div>
            <div class="col-6">
                <button type="button" class="btn btn-lg btn-outline-dark" onclick="showQ('fill')">
                    <i class="fa fa-outdent"></i>
                </button>
                <button type="button" class="btn btn-lg btn-outline-dark" onclick="showQ('quiz')">
                    <i class="fa fa-list-ul"></i>
                </button>
                <button type="button" class="btn btn-lg btn-outline-dark" onclick="showQ('typeIn')">
                    <i class="fa fa-terminal"></i>
                </button>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-12">
                <form action="{{ route('topic.store') }}" method="POST" id="form" onsubmit="return getContent()">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <input type="hidden" name="lesson_id" value="{{ $lesson->id }}">
                            <input type="hidden" name="active" id="active" value="">
                            <div class="form-group">
                                <label for="topic_body"><b>KONU</b></label>
                                <textarea rows="20" name="topic_body" class="form-control" id="topic" placeholder="Konu anlatımı içeriği">{{ old('topic_body') }}</textarea>
                                <p><small>{{ $errors->first('body') }}</small></p>
                            </div>
                        </div>
                        <div class="col-6">
                            <input type="hidden" name="lesson_id" value="{{ $lesson->id }}">
                            <label for="question_body"><b>SORU</b></label>
                            <div class="q_outer">
                                <div id="content" contenteditable>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    A, ad fugit maiores optio placeat tenetur totam! Architecto, asperiores dolor facere
                                    incidunt laborum omnis praesentium rem sapiente sunt! Fugiat, illum, ullam?
                                </div>
                                <textarea name="question_body" id="myTextArea" style="display: none;"></textarea>
                            </div>

                            <div id="answers">

                            </div>

                        </div>
                    </div>
                    <hr>
                    <input type="hidden" name="position" value="0">
                    <button type="submit" class="btn btn-primary w-100" id="submit">Ekle</button>
                </form>
            </div>
        </div>
    </div>


    <div id="fill">
        <input type="hidden" name="question_type" value="FILL">
        <input type="hidden" name="marked" id="marked" value="">
        <p class="text-dark pt-3"><b>Cevap olarak kalması gereken yerleri 'MARK' butonuyla işaretleyiniz.</b></p>
        <button type="button" class="btn btn-warning" onclick="mark()">MARK</button>
    </div>

    <div id="quiz">
        <input type="hidden" name="question_type" value="QUIZ">
        <p class="text-dark pt-3"><b>Üç seçenek yazıp doğru cevabı işaretleyiniz</b></p>

        <div class="input-group-prepend">
            <div class="input-group-text">
                <input type="radio" name="answer" value="A">
            </div>
            <input type="text" name="answers[]" class="form-control" placeholder="Seçenek 1" required>
        </div>

        <div class="input-group-prepend">
            <div class="input-group-text">
                <input type="radio" name="answer" value="B">
            </div>
            <input type="text" name="answers[]" class="form-control" placeholder="Seçenek 2" required>
        </div>

        <div class="input-group-prepend">
            <div class="input-group-text">
                <input type="radio" name="answer" value="C">
            </div>
            <input type="text" name="answers[]" class="form-control" placeholder="Seçenek 3" required>
        </div>
    </div>

    <div id="typeIn">
        <input type="hidden" name="question_type" value="TYPEIN">
        <p>Bir soru içeriği ve bir cevap giriniz.</p>
        <div class="form-group">
            <input type="text" name="answer" class="form-control" placeholder="Doğru Cevap" required>
        </div>
    </div>



@endsection

@section('js')
    <script>tinymce.init({selector:'#topic'});</script>
@endsection


