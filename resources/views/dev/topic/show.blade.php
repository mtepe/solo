@extends('dev.layouts.app')

@section('title', $topic->name)

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6 card">
                <div class="card-header d-flex justify-content-between">
                    <p><b>KONU</b></p>
                    <div class="d-flex">
                        <a href="{{ route('topic.edit', ['topic' => $topic]) }}" class="btn btn-outline-info"><i class="fa fa-edit"></i></a>
                        <form action="{{ route('topic.destroy', ['topic' => $topic]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger ml-2"><i class="fa fa-trash"></i></button>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    <div><?= $topic->body ?></div>
                </div>
            </div>
            <div class="col-6 card">
                <div class="card-header d-flex justify-content-between questionHeader">
                    <p><b>SORU</b></p>
                    <button type="button" class="btn btn-outline-danger" onclick="questionDelete()"><i class="fa fa-trash"></i></button>
                </div>
                <div class="card-body questionBody">
                    <?php echo $topic->question->body; ?>
                    <hr>
                    <p><b>Cevaplar:</b></p>
                    <ol type="a">
                        @foreach($topic->question->answers as $answer)
                            <li>{{ $answer->body }}</li>
                        @endforeach
                    </ol>
                    @if($topic->question->answer)
                        <p><b>Doğru cevap:</b> <mark>{{ $topic->question->answer }}</mark></p>
                    @endif
                </div>

                <p class="text-danger"><small>{{ $errors->first('question_body') }}</small></p>
                <p class="text-danger"><small>{{ $errors->first('active') }}</small></p>
                <p class="text-danger"><small>{{ $errors->first('marked') }}</small></p>
                <p class="text-danger"><small>{{ $errors->first('answer') }}</small></p>
                <p class="text-danger"><small>{{ $errors->first('answers') }}</small></p>
            </div>
        </div>
    </div>

    <div class="buttons" style="display: none;">
        <button type="button" class="btn btn-lg btn-outline-dark" onclick="showQ('fill')">
            <i class="fa fa-outdent"></i>
        </button>
        <button type="button" class="btn btn-lg btn-outline-dark" onclick="showQ('quiz')">
            <i class="fa fa-list-ul"></i>
        </button>
        <button type="button" class="btn btn-lg btn-outline-dark" onclick="showQ('typeIn')">
            <i class="fa fa-terminal"></i>
        </button>
    </div>

    <div id="formBody"  style="display: none;">
        <form action="{{ route('question.update', ['question' => $topic->question]) }}" method="POST" id="form" onsubmit="return getContent()">
            @csrf
            @method('PATCH')
            <input type="hidden" name="active" id="active">
            <div class="q_outer">
                <div id="content" contenteditable>
                    {{ old('question_body') }}
                </div>
                <textarea name="question_body" id="myTextArea" style="display: none;"   ></textarea>
            </div>

            <div id="answers">

            </div>

            <hr>
            <button type="submit" class="btn btn-primary w-100" id="submit">Ekle</button>
        </form>
    </div>


    <div id="fill">
        <input type="hidden" name="question_type" value="1">
        <input type="hidden" name="marked" id="marked">
        <p class="text-dark pt-3"><b>Cevap olarak kalması gereken yerleri 'MARK' butonuyla işaretleyiniz.</b></p>
        <button type="button" class="btn btn-warning" onclick="mark()">MARK</button>
    </div>

    <div id="quiz">
        <input type="hidden" name="question_type" value="2">
        <p class="text-dark pt-3"><b>Üç seçenek yazıp doğru cevabı işaretleyiniz</b></p>

        <div class="input-group-prepend">
            <div class="input-group-text">
                <input type="radio" name="answer" value="a" required>
            </div>
            <input type="text" name="answers[]" class="form-control" placeholder="Seçenek 1" required>
        </div>

        <div class="input-group-prepend">
            <div class="input-group-text">
                <input type="radio" name="answer" value="b">
            </div>
            <input type="text" name="answers[]" class="form-control" placeholder="Seçenek 2" required>
        </div>

        <div class="input-group-prepend">
            <div class="input-group-text">
                <input type="radio" name="answer" value="c">
            </div>
            <input type="text" name="answers[]" class="form-control" placeholder="Seçenek 3" required>
        </div>
    </div>

    <div id="typeIn">
        <input type="hidden" name="question_type" value="3">
        <p>Bir soru içeriği ve bir cevap giriniz.</p>
        <div class="form-group">
            <input type="text" name="answer" class="form-control" placeholder="Doğru Cevap" required>
        </div>
    </div>


@endsection


