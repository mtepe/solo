@extends('dev.layouts.app')

@section('title', $topic->lesson->name.'\'e Konu Anlatımı Düzenle')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-6">
                <h2>{{ $topic->lesson->name }} - Konu Anlatımı Düzenle </h2>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-12">
                <form action="{{ route('topic.update', ['topic' => $topic]) }}" method="POST"->
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="body"><b>KONU</b></label>
                                <textarea rows="20" name="body" class="form-control" id="topic">{{ old('topic_body') ?? $topic->body }}</textarea>
                                <p class="text-danger"><b>{{ $errors->first('body') }}</b></p>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary w-100">KAYDET</button>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>tinymce.init({selector:'#topic'});</script>
@endsection
