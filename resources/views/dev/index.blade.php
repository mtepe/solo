@extends('dev.layouts.app')

@section('title', 'Yönetici Paneli')

@section('content')
    <div class="container">
            <p>Geliştirici Paneline Hoşgeldiniz</p>

            <hr>

            <a href="{{ route('tutorial.index') }}">Kurslar</a>
    </div>
@endsection
