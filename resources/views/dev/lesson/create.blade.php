@extends('dev.layouts.app')

@section('title', $module->name.' Modülüne Ders Ekle')

@section('content')
    <div class="container">
            <h2>{{ $module->name }} Modülüne Ders Ekle</h2>
            <form action="{{ route('lesson.store') }}" method="POST">
                @csrf
                <input type="hidden" name="module_id" value="{{ $module->id }}">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Ders ismi" required>
                    <p><small>{{ $errors->first('name') }}</small></p>
                </div>
                <input type="hidden" name="position" value="0">
                <button type="submit" class="btn btn-primary">Ekle</button>
            </form>
    </div>
@endsection
