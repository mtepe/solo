@extends('dev.layouts.app')

@section('title', $lesson->name.' Dersi Düzenleniyor')

@section('content')
    <div class="container">
        <h2>{{ $lesson->name }} Dersi Düzenle</h2>
        <form action="{{ route('lesson.update', ['lesson' => $lesson]) }}" method="POST">
            @csrf
            @method('PATCH')
            <input type="hidden" name="module_id" value="{{ $lesson->module->id }}">
            <div class="form-group">
                <input type="text" name="name" value="{{ $lesson->name }}" class="form-control" placeholder="Ders ismi" required>
                <p><small>{{ $errors->first('name') }}</small></p>
            </div>
            <button type="submit" class="btn btn-primary">Düzenle</button>
        </form>
    </div>
@endsection
