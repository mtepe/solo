@extends('dev.layouts.app')

@section('title', $lesson->name.' Dersi')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 mb-2">
                <h2>{{ $lesson->name }}</h2>
                <p>
                    <b>Modül: </b>
                    <a href="{{ route('module.show', ['module' => $lesson->module]) }}">
                        {{ $lesson->module->name }}
                    </a>
                </p>
                <p><a href="{{ route('lesson.edit', ['lesson' => $lesson]) }}" class="btn btn-info">Düzenle</a></p>
                <form action="{{ route('lesson.destroy', ['lesson' => $lesson]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="button" onclick="if(confirm('Silmek istiyor musun?')){$(this).parent().submit()}"
                            class="btn btn-danger">Sil</button>
                </form>
            </div>
            <div class="col-md-8 mb-2">
                <h3>Konu Anlatımları</h3>
                <p>
                    <a href="{{ route('topic.create', ['lesson' => $lesson]) }}" class="btn btn-sm btn-outline-secondary">
                        Yeni Konu Anlatımı
                    </a>
                </p>

                <table>
                    <tbody>
                        @foreach($lesson->topics()->orderBy('position', 'asc')->get() as $topic)
                            <tr data-index="{{ $topic->id }}" data-position="{{ $topic->position }}">
                                <td>
                                    <a href="{{ route('topic.show', ['topic' => $topic]) }}">
                                        {{ $topic->position .' - '. substr(strip_tags($topic->body), 0, 40) . '...' }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('table tbody').sortable({
                update: function (event, ui) {
                    $(this).children().each(function (index) {
                        if($(this).attr('data-position') != (index+1)) {
                            $(this).attr('data-position', (index+1)).addClass('updated');
                        }
                    });

                    saveNewPositions();
                }
            });

        });

        function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
                positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
                $(this).removeClass('updated');
            });

            $.ajax({
                url: '{{ route('topic.updatePos') }}',
                method: 'post',
                dataType: 'text',
                data: {
                    update: 1,
                    positions: positions,
                    _token: '{{csrf_token()}}',
                }, success: function (response) {
                    console.log(response);
                }
            });
        }
    </script>
@endsection
