@extends('layouts.app')
@section('title', $tutorial->name.' Kursu - Eğitben')

@section('content')

    <div class="all-courses section-padding animated fadeIn">
        <div class="container py-5">
            <div class="row">

                <!-- a course -->
                <div class="col-lg-4 mb-2">
                    <div class="a-course">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ asset('img/courses/'.strtolower($tutorial->name).'.png') }}" class="w-100">
                                <a href="{{ route('play', ['tutorial' => 1]) }}" class="btn btn-sm btn-outline-info mt-3">Başla</a>
                            </div>
                            <div class="col-9">
                                <h4>{{ $tutorial->name }} Temelleri</h4>
                                <p>
                                    Bu ücretsiz kurs size HTML ile nasıl web sayfası tasarlayabileceğinizi öğretir.
                                    Bir çok pratik ve konuyla birlikte gerçek bir HTML sayfası yazmayı hemen öğrenebilirsiniz.
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /a course/ -->

                <div class="col-lg-8">
                    <div class="course-accord" id="accordion">

                        <div class="d-flex justify-content-between py-3">
                            <h5 id="ders">Ders Sayısı: 44</h5>
                            <h5 id="soru"><i class="fa fa-question-circle mr-1"></i>125 Soru</h5>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                    Modül 1: HTML Genel Bakış
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                <div class="card-body">
                                    <ol>
                                        <li>HTML Nedir ?</li>
                                        <li>Temel HTML Yapısı</li>
                                        <li>İlk HTML Sayfanız</li>
                                        <li>Blog Yapımı</li>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                    Modül 2: HTML Temelleri
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <ol>
                                        <li>Paragraflar</li>
                                        <li>Yazı Biçimlendirme</li>
                                        <li>Başlıklar ve Yorumlar</li>
                                        <li>Elementler</li>
                                        <li>Özellikler</li>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                    Modül 3: HTML İleri
                                </a>
                            </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    Lorem ipsum..
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
