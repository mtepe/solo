@extends('layouts.app')

@section('content')
<div class="login section-padding animated fadeIn">
    <div class="container py-5">
        <div class="row">

            <div class="col-lg-8">
                <div class="form myBox">
                    <div class="d-flex justify-content-between">
                        <h2>Giriş</h2>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <input id="email" type="email" placeholder="Eposta" name="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" placeholder="Şifre" name="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                    required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-check  d-flex justify-content-between">
                            <div>
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Beni Hatırla') }}
                                </label>
                            </div>
                            @if (Route::has('password.request'))
                            <div>
                                <a href="{{ route('password.request') }}" class="text-small text-muted">
                                    {{ __('Şifremi Unuttum') }}
                                </a>
                            </div>
                            @endif
                        </div>
                        <div class="mt-4 d-flex justify-content-between">
                            <button type="submit" class="btn btn-success w-75 btn-lg mr-3">Giriş</button>
                            <a href="{{ route('register') }}" class="btn btn-lg btn-outline-secondary w-25">
                                {{ __('Kayıt') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-4 logo-container"></div>

        </div>
    </div>
</div>
@endsection
