@extends('layouts.app')
@section('title', 'Yeni Şifre Belirle - Egitben')

@section('content')
<div class="login section-padding animated fadeIn">
    @if (session('status'))
        <div class="alert alert-success text-center p-sm-5" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="container py-5">
        <div class="row">

            <div class="col-lg-8">
                <div class="form myBox">
                    <div class="d-flex justify-content-between">
                        <h2>Yeni Şifre Belirle</h2>
                    </div>

                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <input id="email" type="email" name="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   value="{{ $email ?? old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" name="password" placeholder="Yeni şifre"
                                   class="form-control @error('password') is-invalid @enderror"
                                    required autocomplete="new-password" autofocus>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" name="password_confirmation"
                                   placeholder="Yeni şifre tekrar" class="form-control"  required autocomplete="new-password">
                        </div>

                        <div class="mt-4 d-flex justify-content-between">
                            <button type="submit" class="btn btn-success w-100">
                                {{ __('Şifre Yenile') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-4 logo-container"></div>

        </div>
    </div>
</div>
@endsection
