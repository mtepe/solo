@extends('layouts.app')
@section('title', 'Şifre Sıfırlama Maili Gönder - Egitben')

@section('content')
<div class="login section-padding animated fadeIn">
    @if (session('status'))
        <div class="alert myAlert text-center py-5" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="container py-5">
        <div class="row">

            <div class="col-lg-8">
                <div class="form myBox">
                    <div class="d-flex justify-content-between">
                        <h2>Şifre Sıfırlama Maili Gönder</h2>
                    </div>

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                            <input id="email" type="email" placeholder="Eposta" name="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="mt-4 d-flex justify-content-between">
                            <button type="submit" class="btn btn-success w-100">
                                {{ __('Şifre Sıfırlama Linki Gönder') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-4 logo-container"></div>

        </div>
    </div>
</div>
@endsection
