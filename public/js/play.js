var count = 0;
var data;
var key;
var spinner = '<div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div>' +
    '<div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div>' +
    '<div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div>' +
    '<div class="sk-cube sk-cube9"></div></div>';

// On click module
$(document).on('click', '.module', function() {
    var url = $(this).attr('data-target');
    load_areas(url);
});

// On click lesson
$(document).on('click', '.lesson', function() {
    var url = $(this).attr('data-target');
    load_topic(url);
});

// On click skip
$(document).on('click', '#skip', function() {
    // Topic key from skip button
    var key = $(this).attr('data-key');

    $('.tab').removeClass('play-next');
    $('#tab'+key+'Q').toggleClass('play-next');

    load_question(key);

});

// On click check
$(document).on('click', '#check', function() {
    var form_id = $(this).attr('data-belongs');
    var form = $('#'+form_id).serializeArray();
    var question_id = form[0]['value'];
    var type = form[1]['value'];
    var answer = form[2]['value'];

    check_question(question_id, type, answer);

});

// On click back button
$(document).on('click', '#backButton', function() {
    var url = $(this).attr('data-target');
    clear();
    load_areas(url);
});

// On click tab
$(document).on('click', '.tab', function () {
    $('.tab').removeClass('play-next');
    $(this).toggleClass('play-next');
    var key = $(this).attr('data-key');
    var data_type = $(this).attr('data-type');
    if(data_type == 'question') {
        load_question(key);
    } else if (data_type == 'topic') {
        $('.play-body').html(data.topics[key]['content']);
        $('.play-footer').html(data.topics[key]['button']);
    }
});

// On click certificate
$(document).on('click', '#certificate', function () {
    var tut = $(this).attr('data-belongs');

    $.ajax({
        type: "get",
        url: '/certificate/check/'+tut,
        success	:function(result)
        {
            window.location = result;

        },
        error:function()
        {
            alert('Sertifikayı görüntüleyebilmek için kursu tamamlayınız');
        },
    });
});

// Load modules and lessons
function load_areas(url){
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function() {
            $('#playInner').html(spinner);
        },
        success	:function(result)
        {
            $('#playInner').html('');
            var data = $.parseJSON(result);
            for (i=0; i < data.areas.length; i++) {
                $('#playInner').append(data.areas[i]);
            }
            $('#playInner').append(data.certificate);
            $('.back-area').html(data.back);

        },
        error:function()
        {
            alert('Bir hata oluştu');
        },
    });
}

// Load topics and links
function load_topic(url) {
    $.ajax({
        type: 'get',
        url: url,
        beforeSend: function() {
            $('#playInner').html(spinner);
        },
        success: function(result) {
            $('#playInner').html('');
            $('.play-body').addClass('playBox');
            $('.tab-list').html('');
            data = $.parseJSON(result);
            for(i=0; i < data.topics.length; i++){
                $('.tab-list').append(data.topics[i]['link']);
                $('.tab-list').append(data.topics[i]['question_link']);
            }
            $('.play-body').append(data.topics[0]['content']);
            $('#tab0').toggleClass('play-next');
            $('.play-footer').html(data.topics[0]['button']);
            $('.back-area').html(data.back);
        },
        error: function() {
            alert('Bir hata oluştu');
        },
    });
}

// Load question
function load_question(key){
    // Load Question body
    $('.play-body').html(data.topics[key]['question']['body']);
    // Append question form to body
    $('.play-body').append(data.topics[key]['question']['form']);

    if(data.topics[key]['question']['type'] == 'FILL') {
        $('.play-body').html(data.topics[key]['question']['form']);
        $('#topic'+key+'QuestionForm').append(data.topics[key]['question']['body']);
    } else if (data.topics[key]['question']['type'] == 'QUIZ') {
        for(i=0; i < (data.topics[key]['question']['answers']).length; i++) {
        //  <label><input type="radio" name="answers" value="'+i+'" class="form-radio">'+data.topics[key]['question']['answers'][i]['body']+'</label>
            $('#topic'+key+'QuestionForm').append('<label><input type="radio" name="answers" value="'+i+'" class="form-radio">'+data.topics[key]['question']['answers'][i]['body']+'</label>');
        }
    } else if (data.topics[key]['question']['type'] == 'TYPEIN') {
        var len = data.topics[key]['question']['answer'].length;
        $('#topic'+key+'QuestionForm').append('<input type="text" name="answer" size="'+len+'" maxlength="'+len+'" required>');
    }

    // Load check button for question
    $('.play-footer').html(data.topics[key]['question_button']);
}

// Validate question
function check_question(question_id, type, answer) {
    if(type == 'FILL') {

        var request = $("input[name='qs[]']")
            .map(function(){return $(this).val();}).get();

        var check = compare(request, data.topics[question_id]['question']['answers']);

        if (check == true) {
            if(count <= question_id) {
                count += 1;
            }
            swal_success();

        } else {
            swal_danger();
        }

    } else if (type == 'QUIZ') {

        // Switching values to match database
        switch (answer) {
            case '0':
                answer = 'a';
                break;
            case '1':
                answer = 'b';
                break;
            case '2':
                answer = 'c';
                break;
        }

        // Correct answer
        if(answer == data.topics[question_id]['question']['answer']){
            if(count <= question_id) {
                count += 1;
            }
            swal_success();
        }
        // Wrong answer
        else {
            swal_danger();
        }

    } else if (type == 'TYPEIN') {

        // Correct answer
        if(answer == data.topics[question_id]['question']['answer']){
            if(count <= question_id) {
                count += 1;
            }
            swal_success();
        }
        // Wrong answer
        else {
            swal_danger();
        }
    }
}

// Compare request with answers (for question type 1)
function compare(request, answers){
    var result;
    for (i=0; i < answers.length; i++) {
        if (request[i] ==  answers[i]['body']) {
            result = true;
        } else {
            result = false;
            break;
        }
    }

    return result;
}

// Load next topic on validation
function load_next() {
    // If completed, redirect
    if(count >= data.topics.length) {
        clear();
        update_lesson(data.lesson);
    } else {
        $('#tab'+count).removeClass('play-disabled');
        $('#tab'+count+'Q').removeClass('play-disabled');
        $('.play-body').html(data.topics[count]['content']);
        $('.play-footer').html(data.topics[count]['button']);

        $('.tab').removeClass('play-next');
        $('#tab'+count).toggleClass('play-next');
    }

}

// Clear areas
function clear(){
    count = 0;
    $('.back-area').html('');
    $('.tab-list').html('');
    $('#playInner').html('');
    $('.play-body').html('');
    $('.play-body').removeClass('playBox');
    $('.play-footer').html('');
}

// Update lesson
function update_lesson($id){
    var url = '/play/update_lesson/'+data.lesson;
    $.ajax({
        type: 'get',
        url: url,
        success: function(result) {
            load_areas(result);
        },
        error: function() {
            alert('Bir hata oluştu');
        },
    });
}

function swal_success() {
    $('.play-footer').html('');
    Swal.fire({
        title: 'Doğru!',
        icon: 'success',
        showClass: {
            popup: 'animated slideInRight faster'
        },
        hideClass: {
            popup: 'animated fadeOutUp faster'
        }
    }).then(function() {
        load_next();
    });
}

function swal_danger() {
    Swal.fire({
        title: 'Yanlış',
        icon: 'error',
        showClass: {
            popup: 'animated slideInRight faster'
        },
        hideClass: {
            popup: 'animated fadeOutUp faster'
        }
    });
}
