$(document).ready(function() {
    // Transition effect for navbar
    $(window).scroll(function() {
        // checks if window is scrolled more than 500px, adds/removes solid class
        if($(this).scrollTop() > 1) {
            $('.navbar').addClass('solid');
        } else {
            $('.navbar').removeClass('solid');
        }
    });
});

$('.navbar-toggler-icon').click(function () {
    $('.navbar').addClass('solid');
});

$('.header-slider').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    autoplay: true,
    autoplayTimeout: 4000,
    smartSpeed: 1000,
    items: 1,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    center: true,
});

$(document).ready(function() {
    $('.myNav').removeClass('solid');
});
