var base_url  = 'http://127.0.0.1:8000/';

function showQ(type) {
    $('#answers').html($('#' + type).html());
    $('#active').val(1);
}

function mark(){
    $('#marked').val(1);
    var text = window.getSelection().toString();
    document.execCommand("insertHTML", 1, '<mark>'+text+'</mark>')
}

function getContent(){
    var val = $('#content').html();
    $('#myTextArea').val(val);
}

function questionDelete() {
    $('.questionHeader').html($('.buttons').html());
    $('.questionBody').html($('#formBody').html());
}

$(document).ready(function () {

    'use strict';

    var c, currentScrollTop = 0,
        navbar = $('nav');

    $(window).scroll(function () {
        var a = $(window).scrollTop();
        var b = navbar.height();

        currentScrollTop = a;

        if (c < currentScrollTop && a > b + b) {
            navbar.addClass("scrollUp");
        } else if (c > currentScrollTop && !(a <= b)) {
            navbar.removeClass("scrollUp");
        }
        c = currentScrollTop;
    });

});


