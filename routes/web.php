<?php


Auth::routes();

// Home routes
Route::get('/', 'HomeController@index')->name('home');
Route::get('kurslar', 'HomeController@courses')->name('courses');
Route::get('kurs/{tutorial}', 'HomeController@course')->name('course');
Route::get('home/emailSub', 'HomeController@emailSub')->name('home.emailSub');

/* Dev Panel Routes */
Route::get('dev', 'DevController@index')->name('dev.index');

// Dev Tutorial Routes
Route::resource('dev/tutorial', 'Dev\TutorialController');
Route::post('tutorial/updatePos', 'Dev\TutorialController@updatePos')->name('tutorial.updatePos');

// Dev Module Routes
Route::resource('dev/module', 'Dev\ModuleController')->middleware('dev');
Route::get('module/create/{tutorial}', 'Dev\ModuleController@create')->name('module.create');
Route::post('module/updatePos', 'Dev\ModuleController@updatePos')->name('module.updatePos');

// Dev Lesson Routes
Route::resource('dev/lesson', 'Dev\LessonController')->middleware('dev');
Route::get('lesson/create/{module}', 'Dev\LessonController@create')->name('lesson.create');
Route::post('lesson/updatePos', 'Dev\LessonController@updatePos')->name('lesson.updatePos');

// Dev Topic Routes
Route::resource('dev/topic', 'Dev\TopicController')->middleware('dev');
Route::get('topic/create/{lesson}', 'Dev\TopicController@create')->name('topic.create');
Route::post('topic/updatePos', 'Dev\TopicController@updatePos')->name('topic.updatePos');

// Dev Question Routes
Route::resource('dev/question', 'Dev\QuestionController');

/* Play Routes */
Route::get('play/{tutorial}', 'PlayController@index')->name('play');
Route::get('play/reset/{tutorial}', 'PlayController@reset_progress')->name('play.reset');

/* Play Ajax Routes */
Route::get('play/tutorial/{tutorial}', 'PlayController@tutorial')->name('play.tutorial');
Route::get('play/module/{module}', 'PlayController@module')->name('play.module');
Route::get('play/lesson/{lesson}', 'PlayController@lesson')->name('play.lesson');
Route::get('play/update_lesson/{lesson}', 'PlayController@update_lesson')->name('update.lesson');

// Profile Routes
Route::get('profil', 'ProfileController@index')->name('profile.index');
Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

// Certificate Routes
Route::get('certificate/{certificate}', 'CertificateController@show')->name('certificate.show');
Route::get('certificate/check/{tutorial}', 'CertificateController@check')->name('certificate.check');


