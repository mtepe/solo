<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Define users
        $users = [
            [
                'name' => 'Muhammed Tepe',
                'email' => 'developer@egitben.com',
                'password' => 'test1234',
            ],
        ];

        // Create all users
        foreach ($users as $user) {
            $newUser = new \App\User();
            $newUser->name = $user['name'];
            $newUser->email = $user['email'];
            $newUser->password = \Illuminate\Support\Facades\Hash::make($user['password']);
            $newUser->save();

            $newUser->profile()->create([
                'full_name' => $newUser->name,
                'pic' => 'users/default.png',

            ]);
        }
    }
}
