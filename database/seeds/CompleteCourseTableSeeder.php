<?php

use Illuminate\Database\Seeder;

class CompleteCourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$lessons = [
            [
                'module_id' => 1,
                'name' => 'HTML Nedir ?',
                'position' => 1,
            ],
            [
                'module_id' => 1,
                'name' => 'HTML Paragraflar',
                'position' => 2,
            ],
            [
                'module_id' => 1,
                'name' => 'HTML Yazı Biçimlendirme',
                'position' => 3,
            ],
        ];*/

        // Create tutorial
        $tutorial = new \App\Tutorial();
        $tutorial->name = 'HTML';
        $tutorial->logo = 'uploads/html.png';
        $tutorial->position = 1;
        $tutorial->short_desc = 'Bu ücretsiz kurs size HTML ile nasıl web sayfası tasarlayabileceğinizi öğretir.
            Bir çok pratik ve konuyla birlikte gerçek bir HTML sayfası yazmayı hemen öğrenebilirsiniz.';
        $tutorial->long_desc = 'Bu ücretsiz kurs size HTML ile nasıl web sayfası tasarlayabileceğinizi öğretir.
            Bir çok pratik ve konuyla birlikte gerçek bir HTML sayfası yazmayı hemen öğrenebilirsiniz.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat.';
        $tutorial->save();

        // Create module
        $module = new \App\Module();
        $module->tutorial_id = 1;
        $module->name = 'HTML Temel';
        $module->icon = 'uploads/basics.png';
        $module->position = 1;
        $module->save();

        // Create lesson
        $lesson = new \App\Lesson();
        $lesson->module_id = 1;
        $lesson->name = 'HTML Nedir ?';
        $lesson->position = 1;
        $lesson->save();

        // Create topic
        $topicA = new \App\Topic();
        $topicA->lesson_id = 1;
        $topicA->body = '<p>Hiper Metin İşaretleme Dili web sayfalarını oluşturmak için kullanılan standart metin
                        işaretleme dilidir.</p><p>Dilin son sürümü HTML5\'tir. HTML, bir programlama dili olarak tanımlanamaz.
                        </p><p>Zira HTML kodlarıyla kendi başına çalışan bir program yazılamaz.</p>';
        $topicA->position = 1;
        $topicA->save();

        // Create question
        $questionA = new \App\Question();
        $questionA->topic_id = 1;
        $questionA->body = 'HTML\'in açılımı nedir aşağıdakilerden hangisidir ?';
        $questionA->answer = 'b';
        $questionA->type = 'QUIZ';
        $questionA->save();

        // Create answers
        $answerA = new \App\Answer();
        $answerA->question_id = 1;
        $answerA->body = 'How To Meet Ladies';
        $answerA->save();

        $answerB = new \App\Answer();
        $answerB->question_id = 1;
        $answerB->body = 'Hyper Text Markup Language';
        $answerB->save();

        $answerC = new \App\Answer();
        $answerC->question_id = 1;
        $answerC->body = 'How To Meet Expectations';
        $answerC->save();

    }
}
